﻿using UnityEngine;

namespace HexMap
{
    public static class HexMetrics
    {

        public const float outerRadius = 10f;

        public const float innerRadius = outerRadius * 0.866025404f; //0.866... = 0.5*3^(0.5) = ratio of radii

        public static Vector3[] corners =
        {
            new Vector3(0f, 0f, outerRadius),
            new Vector3(innerRadius, 0f, 0.5f * outerRadius),
            new Vector3(innerRadius, 0f, -0.5f * outerRadius),
            new Vector3(0f, 0f, -outerRadius),
            new Vector3(-innerRadius, 0f, -0.5f * outerRadius),
            new Vector3(-innerRadius, 0f, 0.5f * outerRadius),
            new Vector3(0f, 0f, outerRadius)
        };

    }
}