using System;
using System.Runtime.CompilerServices;

namespace MatrixWorkspace
{
    public class Matrix
    {
        public float[,] value { get; private set; }

        public Matrix(float[,] value)
        {
            this.value = value;
        }

        public static Matrix operator +(Matrix matrix1, Matrix matrix2)
        {
            var value1 = matrix1.value;
            var value2 = matrix2.value;

            if (value1.GetLength(0) != value2.GetLength(0) || value1.GetLength(1) != value2.GetLength(1))
                throw new Exception("Matrices has different size");
            
            var sumValue = value1;
            for (var i = 0; i < sumValue.GetLength(0); i++)
            {
                for (var j = 0; j < sumValue.GetLength(1); j++)
                {
                    sumValue[i, j] += value2[i, j];
                }
            }

            return new Matrix(sumValue);

        }
        public static Matrix operator -(Matrix matrix1, Matrix matrix2)
        {
            var value1 = matrix1.value;
            var value2 = matrix2.value;

            if (value1.GetLength(0) != value2.GetLength(0) || value1.GetLength(1) != value2.GetLength(1))
                throw new Exception("Matrices has different size");
            
            var sumValue = value1;
            for (var i = 0; i < sumValue.GetLength(0); i++)
            {
                for (var j = 0; j < sumValue.GetLength(1); j++)
                {
                    sumValue[i, j] -= value2[i, j];
                }
            }

            return new Matrix(sumValue);

        }
        public static Matrix operator *(float number, Matrix matrix)
        {
            var value = matrix.value;
            for (var i = 0; i < value.GetLength(0); i++)
            {
                for (var j = 0; j < value.GetLength(1); j++)
                {
                    value[i, j] *= number;
                }
            }
            
            return new Matrix(value);
        }
        public static Matrix operator *(Matrix matrix, float number)
        {
            var value = matrix.value;
            for (var i = 0; i < value.GetLength(0); i++)
            {
                for (var j = 0; j < value.GetLength(1); j++)
                {
                    value[i, j] *= number;
                }
            }
            
            return new Matrix(value);
        }
        public static Matrix operator *(Matrix matrix1, Matrix matrix2)
        {
            var value1 = matrix1.value;
            var value2 = matrix2.value;

            if (value1.GetLength(0) != value2.GetLength(1))
            {
                throw new Exception("Matrices non-multiply");
            }

            var multiplyValue = new float[value1.GetLength(1),value2.GetLength(0)];

            for (var i = 0; i < value1.GetLength(1); i++)
            {
                for (var j = 0; j < value2.GetLength(0); j++)
                {
                    float sum = 0;
                    for (var k = 0; k < value1.GetLength(0); k++)
                    {
                        sum += value1[k, i] * value2[j, k];
                    }

                    multiplyValue[i, j] = sum;
                }
            }
            
            return new Matrix(multiplyValue);
        }

        public Matrix Transpose()
        {
            var tranValue = new float[value.GetLength(1),value.GetLength(0)];
            for (var i = 0; i < value.GetLength(1); i++)
            {
                for (var j = 0; j < value.GetLength(0); j++)
                {
                    tranValue[i, j] = value[j, i];
                }
            }
            return new Matrix(tranValue);
        }
        
        //TODO: Maybe in future need add more methods but neural network use only Transpose, so...

        public override string ToString()
        {
            var sValue = "{";
            for (int i = 0; i < value.GetLength(0); i++)
            {
                sValue += "{ ";
                
                for (int j = 0; j < value.GetLength(1); j++)
                {
                    sValue += value[i,j] + " ";
                }

                sValue += "}";
            }
            sValue += "}";

            return sValue;
        }
    }
}