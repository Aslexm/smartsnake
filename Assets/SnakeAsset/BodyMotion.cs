﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace SnakeAsset
{
    public class BodyMotion : MonoBehaviour
    {
        [NonSerialized]
        public GameObject Parent;
        [NonSerialized]
        public GameObject Child;
        public GameObject Food;
        
        public float impulseForce;

        public bool potency = true;
        public bool parentIsDead;

        [NonSerialized] public Vector3 newPosition;
        [NonSerialized] public Quaternion newRotation;
        

        void Update()
        {
            //TODO: Make call from head
            if (parentIsDead && transform.position == Parent.transform.position)
            {
                Food = Instantiate(Food, transform.position, Quaternion.identity);
                Food.GetComponent<Rigidbody>().AddForce
                (
                    new Vector3(Random.Range(-2, 2), Random.Range(-2, 2), 0) * impulseForce,
                    ForceMode.Impulse
                );

                if (Child != null)
                {
                    Child.GetComponent<BodyMotion>().parentIsDead = true;
                }

                Destroy(this);
            }
        }


        public void Motion()
        {
            if (!potency)
            {
                Child.GetComponent<BodyMotion>().newRotation = transform.rotation;
                Child.GetComponent<BodyMotion>().newPosition = transform.position;

                Child.GetComponent<BodyMotion>().Motion();
            }
            
            transform.position = newPosition;
            transform.rotation = newRotation;
        }
        
        public void MoreLength()
        {
            if (potency)
            {
                Child = Instantiate(gameObject);
                Child.GetComponent<BodyMotion>().Parent = gameObject;
                potency = false;
            }
            else
            {
                Child.GetComponent<BodyMotion>().MoreLength();
            }
        }
    }
}