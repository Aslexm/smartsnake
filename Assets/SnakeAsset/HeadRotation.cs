﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeAsset
{
	public class HeadRotation : MonoBehaviour
	{

		public float anglePoint = 30;
		public float speedRotation = 1;

		public void myRotate()
		{
			if (Input.GetKey(KeyCode.Mouse0))
			{
				StopAllCoroutines();
				if (Input.mousePosition.x < Screen.width / 2)
				{
					StartCoroutine(Rotation(transform.eulerAngles.y - anglePoint));
				}
				else
				{
					StartCoroutine(Rotation(transform.eulerAngles.y + anglePoint));
				}
			}
		}

		IEnumerator Rotation(float Angle)
		{
			float yRot = transform.eulerAngles.y;
			Vector3 Rot = transform.eulerAngles;

			int direction = -1; //left            		
			float max = Mathf.Infinity;
			float min = Angle;

			if (Angle >= yRot)
			{
				direction = 1; //right        
				min = -Mathf.Infinity;
				max = Angle;
			}

			while (true)
			{
				if (Input.anyKeyDown) yield break;
				float oldZRot = yRot;
				yRot = Mathf.Clamp(yRot + direction * speedRotation * Time.deltaTime, min, max);
				if (yRot == oldZRot) yield break;
				Rot = new Vector3(0, yRot, 0);
				transform.eulerAngles = Rot;
				yield return null;
			}
		}

	}
}