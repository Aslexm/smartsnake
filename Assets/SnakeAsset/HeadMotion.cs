﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace SnakeAsset
{
    public class HeadMotion : MonoBehaviour
    {
        public GameObject Body;
        [NonSerialized]
        public GameObject Child;
        public GameObject Food;

        public float speed, impulseForce;

        [SerializeField] bool potency = true;

        void Update()
        {
            Motion();
        }

        
        public void Motion()
        {
            if (!potency)
            {
                Child.GetComponent<BodyMotion>().newPosition = transform.position;
                Child.GetComponent<BodyMotion>().newRotation = transform.rotation;
                
                Child.GetComponent<BodyMotion>().Motion();
            }
                
            
            transform.position = transform.position += transform.forward * speed * Time.deltaTime;
            GetComponent<HeadRotation>().myRotate();
        }

        public FoodSpawn _FoodSpawn;
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.name == "Food(Clone)")
            {
                
                _FoodSpawn.InstFoods();
                
                if (potency)
                {
                    Child = Instantiate(Body, transform.position, transform.rotation);
                    Child.GetComponent<BodyMotion>().Parent = gameObject;
                    potency = false;
                }
                else
                {
                    Child.GetComponent<BodyMotion>().MoreLength();
                }

                Destroy(other.gameObject);
            }
            else
            {
                Food = Instantiate(Food, transform.position, Quaternion.identity);
                Food.GetComponent<Rigidbody>().AddForce
                (
                    new Vector3(Random.Range(-2, 2), Random.Range(-2, 2), 0) * impulseForce,
                    ForceMode.Impulse
                );

                if (Child != null)
                {
                    Child.GetComponent<BodyMotion>().impulseForce = impulseForce;
                    Child.GetComponent<BodyMotion>().parentIsDead = true;
                }

                Destroy(GetComponent<HeadRotation>());
                Destroy(this);
            }
        }
    }
}