﻿using HexMap;
using UnityEngine;
using Random = UnityEngine.Random;

public class FoodSpawn : MonoBehaviour
{
	public GameObject Food;
	public int foodCount = 1;
	
	public HexGrid _hexGrid;
	private HexCell[] _cells;
	
	private void Start()
	{
		_cells = _hexGrid.cells;
		InstFoods();
	}
	
	public void InstFoods()
	{
		for (int i = 0; i < foodCount; i++)
		{
			int foodHex = Random.Range(0, _cells.Length);
			Instantiate(Food, _cells[foodHex].transform);
		}
	}
}
